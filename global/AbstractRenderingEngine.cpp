#include "AbstractRenderingEngine.hpp"
#include <stdexcept>
#include <iostream>

AbstractRenderingEngine::AbstractRenderingEngine()
{
	m_clock = 0;
	exit_signal = false;
}

void AbstractRenderingEngine::Run()
{
	exit_signal = false;
	if(!Initialize()) throw std::runtime_error("Unable to initialize!");
	m_clock = new sf::Clock();
	IF_DEBUG(
	sf::Clock seconds;
	int frames=0;
	)
	do
	{
		Render(m_clock->restart());
		IF_DEBUG(
		if(seconds.getElapsedTime().asSeconds() >= 1.00)
		{
			std::cout << "FPS: " << frames << std::endl;
			seconds.restart();
			frames = 0;
		}
		else
		{
			++frames;
		}
		)
	} while(!exit_signal);
	delete m_clock;
	Deinitialize();
}
