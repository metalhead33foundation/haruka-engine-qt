#ifndef ABSTRACTRENDERINGENGINE_HPP
#define ABSTRACTRENDERINGENGINE_HPP
#include <SFML/System/Clock.hpp>
#include <SFML/System/Time.hpp>
#include "global.hpp"

class AbstractRenderingEngine
{
private:
	//sf::Clock* m_clock;
	sf::Clock* m_clock;
protected:
	bool exit_signal;
public:
	//typedef void (*DrawFunction)(sf::Time);
	AbstractRenderingEngine();
	virtual bool Initialize() = 0;
	virtual void Render(sf::Time deltaTime) = 0;
	virtual void Deinitialize() = 0;
	void Run();
};

#endif // ABSTRACTRENDERINGENGINE_HPP
