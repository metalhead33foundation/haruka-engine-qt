#-------------------------------------------------
#
# Project created by QtCreator 2017-08-17T18:36:15
#
#-------------------------------------------------

QT       += core gui gui-private
QT += x11extras

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = qtVulkan
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0
include(qmake_modules/findLibraryLinker.pro)
include(qmake_modules/findPhysFS.pro)
include(qmake_modules/findAssimp.pro)
include(qmake_modules/findLibX11.pro)
include(qmake_modules/findSFML.pro)

SOURCES += \
        main.cpp \
        widget.cpp \
    global/AbstractRenderingEngine.cpp \
    global/IndexArray.cpp \
    global/UploadableGpuArray.cpp \
    global/VertexArray.cpp \
    PhysFsWrappers/PhAssimp.cpp \
    PhysFsWrappers/Physfs4Cpp.cpp \
    vk/Device/VulkanAppContainer.cpp \
    vk/Device/VulkanInstance.cpp \
    vk/Device/VulkanLogicalDevice.cpp \
    vk/Device/VulkanPhysicalDevice.cpp \
    vk/Memory/VulkanMemory.cpp \
    vk/Memory/VulkanMemoryAllocator.cpp \
    vk/Memory/VulkanMemoryPartition.cpp \
    vk/Pipeline/VulkanGraphicsPipeline.cpp \
    vk/Pipeline/VulkanPipelineLayout.cpp \
    vk/Pipeline/VulkanRenderPass.cpp \
    vk/Pipeline/VulkanShader.cpp \
    vk/Pipeline/VulkanShaderModule.cpp \
    vk/Pipeline/VulkanSwapChain.cpp \
    vk/AbstractVulkanEngine.cpp \
    vk/Vertex.cpp \
    vk/VkCommandBufferer.cpp \
    vk/VulkanArrayBuffer.cpp \
    vk/VulkanBuffer.cpp \
    vk/VulkanFramebuffer.cpp \
    vk/VulkanResource.cpp \
    vk/VulkanWrapper.cpp \
    QtVkContainer.cpp \
    QtVkEngine.cpp \
    mainwindow.cpp

HEADERS += \
        widget.h \
    global/AbstractRenderingEngine.hpp \
    global/global.hpp \
    global/IndexArray.hpp \
    global/linkedlist.hpp \
    global/UploadableGpuArray.hpp \
    global/VertexArray.hpp \
    PhysFsWrappers/PhAssimp.hpp \
    PhysFsWrappers/Physfs4Cpp.hpp \
    vk/Device/VulkanAppContainer.hpp \
    vk/Device/VulkanInstance.hpp \
    vk/Device/VulkanLogicalDevice.hpp \
    vk/Device/VulkanPhysicalDevice.hpp \
    vk/Memory/VulkanMemory.hpp \
    vk/Memory/VulkanMemoryAllocator.hpp \
    vk/Memory/VulkanMemoryPartition.hpp \
    vk/Pipeline/VulkanGraphicsPipeline.hpp \
    vk/Pipeline/VulkanPipelineLayout.hpp \
    vk/Pipeline/VulkanRenderPass.hpp \
    vk/Pipeline/VulkanShader.hpp \
    vk/Pipeline/VulkanShaderModule.hpp \
    vk/Pipeline/VulkanSwapChain.hpp \
    vk/AbstractVulkanEngine.hpp \
    vk/Vertex.hpp \
    vk/VkCommandBufferer.hpp \
    vk/VulkanArrayBuffer.hpp \
    vk/VulkanBuffer.hpp \
    vk/VulkanFramebuffer.hpp \
    vk/VulkanResource.hpp \
    vk/VulkanWrapper.hpp \
    global/global.hpp \
    QtVkContainer.hpp \
    QtVkEngine.hpp \
    mainwindow.h

FORMS += \
        widget.ui \
    mainwindow.ui
