#ifndef QTVKCONTAINER_HPP
#define QTVKCONTAINER_HPP
#include "vk/Device/VulkanAppContainer.hpp"
#include <QWindow>
#include <QWidget>
#include <qpa/qplatformnativeinterface.h>

DEFINE_CLASS(QtVkContainer)
class QtVkContainer : public Vk::AppContainer
{
private:
	QWidget *m_window;
protected:
	void initializeWindow();
public:
	QtVkContainer(QWidget *w, const std::string& engine_name, uint32_t engine_version, const std::string& appname,
				  uint32_t app_version, CStringVector& desiredExtensions);
	uint getWidth() const { return m_window->size().width(); }
	uint getHeight() const { return m_window->size().height(); }
	QWidget* getWindow() const { return m_window; }

	static sQtVkContainer create(QWidget *w, const std::string& engine_name, uint32_t engine_version, const std::string& appname,
								 uint32_t app_version, CStringVector& desiredExtensions);

	Vk::pAppContainer castBack();
	static Vk::sAppContainer createCast(QWidget *w, const std::string& engine_name, uint32_t engine_version, const std::string& appname,
										uint32_t app_version, CStringVector& desiredExtensions);
};

#endif // QTVKCONTAINER_HPP
