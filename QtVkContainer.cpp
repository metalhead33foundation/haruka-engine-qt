#include "QtVkContainer.hpp"
#include <QGuiApplication>
#ifdef VK_USE_PLATFORM_XCB_KHR
#include <QX11Info>
#endif

void QtVkContainer::initializeWindow()
{
	IF_DEBUG(
		std::cout << "QtVkContainer[" << reinterpret_cast<uint64_t>(this) << "]: Initializing window." << std::endl;
		std::cout << "Qt Window address: [" << static_cast<void*>(m_window) << "].\n";
	)
#if defined(VK_USE_PLATFORM_WIN32_KHR)
	VkWin32SurfaceCreateInfoKHR surface_create_info;
	memset(&surface_create_info, 0, sizeof(surface_create_info));
	surface_create_info.sType = VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR;
	surface_create_info.hinstance = GetModuleHandle(NULL);
	surface_create_info.hwnd = (HWND)QGuiApplication::platformNativeInterface()->nativeResourceForWindow(QByteArrayLiteral("handle"),m_window);
	VkResult result = vkCreateWin32SurfaceKHR(getInstance()->getInstance(), &surface_create_info, NULL, getSurfaceAddr());
#elif defined(VK_USE_PLATFORM_XCB_KHR)
	VkXcbSurfaceCreateInfoKHR vkWindowInfo;
	vkWindowInfo.sType = VK_STRUCTURE_TYPE_XCB_SURFACE_CREATE_INFO_KHR;
	vkWindowInfo.pNext = 0;
	vkWindowInfo.flags = 0;
	//SDL_VERSION(&sdlWindowInfo.info);
	vkWindowInfo.window = m_window->winId();
	IF_DEBUG(
		std::cout << "X11 Display address: [" << static_cast<uint64_t>(vkWindowInfo.window) << "].\n";
	)
	vkWindowInfo.connection = QX11Info::connection();
	//vkWindowInfo.connection = XGetXCBConnection((Display*)sdlWindowInfo.info.x11.display);
	VkResult result = vkCreateXcbSurfaceKHR(getInstance()->getInstance(),&vkWindowInfo,0,getSurfaceAddr());
#elif defined(VK_USE_PLATFORM_XLIB_KHR)
	VkXlibSurfaceCreateInfoKHR vkWindowInfo;
	vkWindowInfo.sType = VK_STRUCTURE_TYPE_XLIB_SURFACE_CREATE_INFO_KHR;
	vkWindowInfo.pNext = 0;
	vkWindowInfo.flags = 0;
	vkWindowInfo.window = m_window->winId();
	vkWindowInfo.dpy = QX11Info::display();
	IF_DEBUG(
		std::cout << "X11 Display address: [" << static_cast<void*>(sdlWindowInfo.info.x11.display) << "].\n";
	)
	VkResult result = vkCreateXlibSurfaceKHR(getInstance()->getInstance(),&vkWindowInfo,0,getSurfaceAddr());
#elif defined(VK_USE_PLATFORM_WAYLAND_KHR)
	VkWaylandSurfaceCreateInfoKHR vkWindowInfo;
	vkWindowInfo.sType = VK_STRUCTURE_TYPE_WAYLAND_SURFACE_CREATE_INFO_KHR;
	vkWindowInfo.pNext = 0;
	vkWindowInfo.flags = 0;
	vkWindowInfo.display = sdlWindowInfo.info.wl.display;
	vkWindowInfo.surface = sdlWindowInfo.info.wl.surface;
	VkResult result = vkCreateWaylandSurfaceKHR(getInstance()->getInstance(),&vkWindowInfo,0,getSurfaceAddr());
#elif defined(VK_USE_PLATFORM_MIR_KHR)
	VkMirSurfaceCreateInfoKHR vkWindowInfo;
	vkWindowInfo.sType = VK_STRUCTURE_TYPE_MIR_SURFACE_CREATE_INFO_KHR;
	vkWindowInfo.pNext = 0;
	vkWindowInfo.flags = 0;
	vkWindowInfo.connection = sdlWindowInfo.info.mir.connection;
	vkWindowInfo.mirSurface = sdlWindowInfo.info.mir.surface;
	VkResult result = vkCreateMirSurfaceKHR(getInstance()->getInstance(),&vkWindowInfo,0,getSurfaceAddr());
#elif defined(VK_USE_PLATFORM_ANDROID_KHR)
	VkAndroidSurfaceCreateInfoKHR vkWindowInfo;
	vkWindowInfo.sType = VK_STRUCTURE_TYPE_ANDROID_SURFACE_CREATE_INFO_KHR;
	vkWindowInfo.pNext = 0;
	vkWindowInfo.flags = 0;
	vkWindowInfo.window = sdlWindowInfo.info.android.window;
	VkResult result = vkCreateAndroidSurfaceKHR(getInstance()->getInstance(),&vkWindowInfo,0,getSurfaceAddr());
#endif
	if(result != VK_SUCCESS)
	{
		switch(result)
		{
		case VK_ERROR_OUT_OF_HOST_MEMORY :
			{
				throw std::runtime_error("Couldn't create the window surface - out of host memory!");
				break;
			}
		case VK_ERROR_OUT_OF_DEVICE_MEMORY :
			{
				throw std::runtime_error("Couldn't create the window surface - out of device memory!");
				break;
			}
		default:
			{
				throw std::runtime_error("Couldn't create the window surface!");
				break;
			}
		}
	}
		IF_DEBUG(
			std::cout << "QtVkContainer[" << static_cast<void*>(this) << "]: Successfully initialized window." << std::endl;
		)
}
QtVkContainer::QtVkContainer(QWidget *w, const std::string& engine_name, uint32_t engine_version, const std::string& appname,
			  uint32_t app_version, CStringVector& desiredExtensions)
{
	m_window = w;
	CStringVector extensionVector;
	extensionVector.push_back("VK_KHR_surface");
#if defined(VK_USE_PLATFORM_WIN32_KHR)
	extensionVector.push_back("VK_KHR_win32_surface");
#elif defined(VK_USE_PLATFORM_XCB_KHR)
	extensionVector.push_back("VK_KHR_xcb_surface");
#elif defined(VK_USE_PLATFORM_XLIB_KHR)
	extensionVector.push_back("VK_KHR_xlib_surface");
#elif defined(VK_USE_PLATFORM_WAYLAND_KHR)
	extensionVector.push_back("VK_KHR_wayland_surface");
#elif defined(VK_USE_PLATFORM_MIR_KHR)
	extensionVector.push_back("VK_KHR_mir_surface");
#elif defined(VK_USE_PLATFORM_ANDROID_KHR)
	extensionVector.push_back("VK_KHR_android_surface");
#endif
	for(CStringIterator it = desiredExtensions.begin();it != desiredExtensions.end();++it)
		{
			extensionVector.push_back(*it);
		}
		CStringVector layerVector;
		createInstance(NULL, appname.c_str(),app_version, engine_name.c_str() , engine_version,
							VK_API_VERSION_1_0, NULL, 0,validationLayers,extensionVector,NULL);
		initializeWindow();
}

sQtVkContainer QtVkContainer::create(QWidget *w, const std::string& engine_name, uint32_t engine_version, const std::string& appname,
							 uint32_t app_version, CStringVector& desiredExtensions)
{
	return sQtVkContainer(new QtVkContainer(w, engine_name, engine_version,appname,app_version,
													 desiredExtensions));
}

Vk::pAppContainer QtVkContainer::castBack()
{
	return static_cast<Vk::pAppContainer>(this);
}
Vk::sAppContainer QtVkContainer::createCast(QWidget *w, const std::string& engine_name, uint32_t engine_version, const std::string& appname,
									uint32_t app_version, CStringVector& desiredExtensions)
{
	return std::dynamic_pointer_cast<Vk::AppContainer>(create(w, engine_name, engine_version,appname,app_version,
														 desiredExtensions));
}
