#ifndef LOGICALDEVICE_HPP
#define LOGICALDEVICE_HPP
#include "VulkanPhysicalDevice.hpp"
namespace Vk {

DEFINE_CLASS(LogicalDevice)
class LogicalDevice
{
private:
	sPhysicalDevice physicalDevice;
	VkDevice device;
	VkQueue graphicsQueue;
	VkQueue presentQueue;
	void index(AppContainer& app);
	void index(AppContainer* app);
public:
	LogicalDevice(AppContainer& app);
	LogicalDevice(AppContainer* app);
	static sLogicalDevice create(AppContainer& app);
	static sLogicalDevice create(AppContainer* app);
	~LogicalDevice();
	sPhysicalDevice getPhysicalDevice() const;
	const VkDevice& getDevice() const;
	const VkQueue& getGraphicsQueue() const;
	VkQueue* getGraphicsQueueAddr();
	const VkQueue& getPresentQueue() const;
	VkQueue* getPresentQueueAddr();
	VkDevice* getDeviceAddress();
};

}
#endif // LOGICALDEVICE_HPP
