#ifndef PHYSICALDEVICE_HPP
#define PHYSICALDEVICE_HPP
#include "VulkanAppContainer.hpp"
namespace Vk {

DEFINE_CLASS(PhysicalDevice)
class PhysicalDevice
{
private:
	VkPhysicalDevice physicalDevice;
	VkPhysicalDeviceMemoryProperties memProperties;
public:

	struct QueueFamilyIndices {
		int graphicsFamily = -1;
		int presentFamily = -1;
		bool isComplete()
		{
			return graphicsFamily >= 0 && presentFamily >= 0;
		}
	};
	struct SwapChainSupportDetails {
			VkSurfaceCapabilitiesKHR capabilities;
			std::vector<VkSurfaceFormatKHR> formats;
			std::vector<VkPresentModeKHR> presentModes;
	};
	struct SwapChainDetails {
			VkExtent2D extent;
			VkSurfaceFormatKHR format;
			VkPresentModeKHR presentMode;
			VkSurfaceCapabilitiesKHR capabilities;
	};

	static const CStringVector deviceExtensions;
	static void pickPhysicalDevice(VkInstance instance, VkSurfaceKHR surface, VkPhysicalDevice* dev);
	static QueueFamilyIndices findQueueFamilies(VkPhysicalDevice device, VkSurfaceKHR surface);
	static bool isDeviceSuitable(VkPhysicalDevice device, VkSurfaceKHR surface);
	static bool checkDeviceExtensionSupport(VkPhysicalDevice device);
	static SwapChainSupportDetails querySwapChainSupport(VkPhysicalDevice device,VkSurfaceKHR surface);
	static VkSurfaceFormatKHR chooseSwapSurfaceFormat(const std::vector<VkSurfaceFormatKHR>& availableFormats);
	static VkPresentModeKHR chooseSwapPresentMode(const std::vector<VkPresentModeKHR>& availablePresentModes);
	static VkExtent2D chooseSwapExtent(const VkSurfaceCapabilitiesKHR& capabilities,uint32_t WIDTH, uint32_t HEIGHT);
	static SwapChainDetails getSwapDetails(VkPhysicalDevice device,VkSurfaceKHR surface,uint32_t WIDTH, uint32_t HEIGHT);

	PhysicalDevice(AppContainer& app);
	PhysicalDevice(AppContainer* app);
	static sPhysicalDevice create(AppContainer& app);
	static sPhysicalDevice create(AppContainer* app);
	const VkPhysicalDevice& getPhysicalDevice() const;
	VkPhysicalDevice* getPhysicalDeviceAddr();

	SwapChainSupportDetails querySwapThis(VkSurfaceKHR surface);
	SwapChainSupportDetails querySwapThis(AppContainer& app);
	SwapChainSupportDetails querySwapThis(AppContainer* app);
	SwapChainDetails getSwapDetailsThis(AppContainer& app);
	SwapChainDetails getSwapDetailsThis(AppContainer* app);
	QueueFamilyIndices findQueueFamiliesForThis(VkSurfaceKHR surface);
	QueueFamilyIndices findQueueFamiliesForThis(AppContainer& app);
	QueueFamilyIndices findQueueFamiliesForThis(AppContainer* app);

	const VkPhysicalDeviceMemoryProperties& getMemoryProperties() const;
	uint32_t findMemoryType(uint32_t typeFilter, VkMemoryPropertyFlags properties);
};

}
#endif // PHYSICALDEVICE_HPP
