#include "VulkanLogicalDevice.hpp"
namespace Vk {

LogicalDevice::LogicalDevice(AppContainer& app)
	: physicalDevice(PhysicalDevice::create(app))
{
	index(app);
}
LogicalDevice::LogicalDevice(AppContainer* app)
	: physicalDevice(PhysicalDevice::create(app))
{
	index(app);
}
sLogicalDevice LogicalDevice::create(AppContainer& app)
{
	return sLogicalDevice(new LogicalDevice(app));
}
sLogicalDevice LogicalDevice::create(AppContainer* app)
{
	return sLogicalDevice(new LogicalDevice(app));
}
void LogicalDevice::index(AppContainer& app)
{
	IF_DEBUG(
		std::cout << "Vk::LogicalDevice[" << static_cast<void*>(this) << "]: Indexing logical device" << std::endl;
	)
	PhysicalDevice::QueueFamilyIndices indices = PhysicalDevice::findQueueFamilies(physicalDevice->getPhysicalDevice(),app.getSurface());
	std::vector<VkDeviceQueueCreateInfo> queueCreateInfos;
	std::set<int> uniqueQueueFamilies = {indices.graphicsFamily, indices.presentFamily};

	float queuePriority = 1.0f;
	for (int queueFamily : uniqueQueueFamilies) {
		VkDeviceQueueCreateInfo queueCreateInfo = {};
		queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
		queueCreateInfo.queueFamilyIndex = queueFamily;
		queueCreateInfo.queueCount = 1;
		queueCreateInfo.pQueuePriorities = &queuePriority;
		queueCreateInfos.push_back(queueCreateInfo);
	}

	VkPhysicalDeviceFeatures deviceFeatures = {};

	VkDeviceCreateInfo createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;

	createInfo.queueCreateInfoCount = static_cast<uint32_t>(queueCreateInfos.size());
	createInfo.pQueueCreateInfos = queueCreateInfos.data();

	createInfo.pEnabledFeatures = &deviceFeatures;

	createInfo.enabledExtensionCount = static_cast<uint32_t>(PhysicalDevice::deviceExtensions.size());
	createInfo.ppEnabledExtensionNames = PhysicalDevice::deviceExtensions.data();

	if (AppContainer::enableValidationLayers) {
		createInfo.enabledLayerCount = static_cast<uint32_t>(AppContainer::validationLayers.size());
		createInfo.ppEnabledLayerNames = AppContainer::validationLayers.data();
	} else {
			createInfo.enabledLayerCount = 0;
	}

	if (vkCreateDevice(physicalDevice->getPhysicalDevice(), &createInfo, nullptr, &device) != VK_SUCCESS)
	{
		throw std::runtime_error("Failed to create logical device!");
	}

	vkGetDeviceQueue(device, indices.graphicsFamily, 0, &graphicsQueue);
	vkGetDeviceQueue(device, indices.presentFamily, 0, &presentQueue);
	IF_DEBUG(
		std::cout << "Vk::LogicalDevice[" << static_cast<void*>(this) << "]: Succesfully indexed." << std::endl;
	)
}
void LogicalDevice::index(AppContainer* app)
{
	index(*app);
}

LogicalDevice::~LogicalDevice()
{
	IF_DEBUG(
		std::cout << "Vk::LogicalDevice[" << static_cast<void*>(this) << "]: Destroying logical device." << std::endl;
	)
	vkDestroyDevice(device,NULL);
	IF_DEBUG(
		std::cout << "Vk::LogicalDevice[" << static_cast<void*>(this) << "]: Destroyed logical device." << std::endl;
	)
}
sPhysicalDevice LogicalDevice::getPhysicalDevice() const
{
	return physicalDevice;
}
const VkDevice& LogicalDevice::getDevice() const
{
	return device;
}
VkDevice* LogicalDevice::getDeviceAddress()
{
	return &device;
}
const VkQueue& LogicalDevice::getGraphicsQueue() const
{
	return graphicsQueue;
}
VkQueue* LogicalDevice::getGraphicsQueueAddr()
{
	return &graphicsQueue;
}
const VkQueue& LogicalDevice::getPresentQueue() const
{
	return presentQueue;
}
VkQueue* LogicalDevice::getPresentQueueAddr()
{
	return &presentQueue;
}

}
