#include "VulkanPhysicalDevice.hpp"

namespace Vk {

const CStringVector PhysicalDevice::deviceExtensions = {
	VK_KHR_SWAPCHAIN_EXTENSION_NAME
};

PhysicalDevice::PhysicalDevice(AppContainer& app)
{
	pickPhysicalDevice(app.getInstance()->getInstance(),app.getSurface(),&physicalDevice);
	vkGetPhysicalDeviceMemoryProperties(physicalDevice, &memProperties);
}
PhysicalDevice::PhysicalDevice(AppContainer* app)
{
	pickPhysicalDevice(app->getInstance()->getInstance(),app->getSurface(),&physicalDevice);
	vkGetPhysicalDeviceMemoryProperties(physicalDevice, &memProperties);
}
sPhysicalDevice PhysicalDevice::create(AppContainer& app)
{
	return sPhysicalDevice(new PhysicalDevice(app));
}
sPhysicalDevice PhysicalDevice::create(AppContainer* app)
{
	return sPhysicalDevice(new PhysicalDevice(app));
}
PhysicalDevice::SwapChainSupportDetails PhysicalDevice::querySwapThis(VkSurfaceKHR surface)
{
	return querySwapChainSupport(physicalDevice,surface);
}
PhysicalDevice::SwapChainSupportDetails PhysicalDevice::querySwapThis(AppContainer& app)
{
	return querySwapThis(app.getSurface());
}
PhysicalDevice::SwapChainSupportDetails PhysicalDevice::querySwapThis(AppContainer* app)
{
	return querySwapThis(app->getSurface());
}

const VkPhysicalDevice& PhysicalDevice::getPhysicalDevice() const
{
	return physicalDevice;
}
VkPhysicalDevice* PhysicalDevice::getPhysicalDeviceAddr()
{
	return &physicalDevice;
}

void PhysicalDevice::pickPhysicalDevice(VkInstance instance, VkSurfaceKHR surface, VkPhysicalDevice* dev)
{
	uint32_t deviceCount = 0;
	vkEnumeratePhysicalDevices(instance, &deviceCount, nullptr);

	if (deviceCount == 0)
	{
		throw std::runtime_error("failed to find GPUs with Vulkan support!");
	}

	std::vector<VkPhysicalDevice> devices(deviceCount);
	vkEnumeratePhysicalDevices(instance, &deviceCount, devices.data());

	for (const auto& device : devices)
	{
		if (isDeviceSuitable(device,surface))
		{
		*dev = device;
		break;
		}
	}

	if (*dev == VK_NULL_HANDLE)
	{
		throw std::runtime_error("failed to find a suitable GPU!");
	}
}
PhysicalDevice::QueueFamilyIndices PhysicalDevice::findQueueFamilies(VkPhysicalDevice device, VkSurfaceKHR surface)
{
	IF_DEBUG(
				std::cout << "Succesfully removed all elements from the list.\n";
	)
	PhysicalDevice::QueueFamilyIndices indices;

	uint32_t queueFamilyCount = 0;
	vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, nullptr);

	std::vector<VkQueueFamilyProperties> queueFamilies(queueFamilyCount);
	vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, queueFamilies.data());

	int i = 0;
	for (const auto& queueFamily : queueFamilies)
	{
		if (queueFamily.queueCount > 0 && queueFamily.queueFlags & VK_QUEUE_GRAPHICS_BIT) {
		indices.graphicsFamily = i;
		}

		VkBool32 presentSupport = false;
		vkGetPhysicalDeviceSurfaceSupportKHR(device, i, surface, &presentSupport);

		if (queueFamily.queueCount > 0 && presentSupport)
		{
			indices.presentFamily = i;
		}

		if (indices.isComplete())
		{
			break;
		}

		i++;
	}

	return indices;
}
bool PhysicalDevice::isDeviceSuitable(VkPhysicalDevice device, VkSurfaceKHR surface)
{
	QueueFamilyIndices indices = findQueueFamilies(device,surface);
	bool extensionsSupported = checkDeviceExtensionSupport(device);
	bool swapChainAdequate = false;
	if (extensionsSupported) {
		SwapChainSupportDetails swapChainSupport = querySwapChainSupport(device,surface);
		swapChainAdequate = !swapChainSupport.formats.empty() && !swapChainSupport.presentModes.empty();
	}
	return indices.isComplete() && extensionsSupported;
}
bool PhysicalDevice::checkDeviceExtensionSupport(VkPhysicalDevice device)
{
	uint32_t extensionCount;
	vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, nullptr);

	std::vector<VkExtensionProperties> availableExtensions(extensionCount);
	vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, availableExtensions.data());

	std::set<std::string> requiredExtensions(deviceExtensions.begin(), deviceExtensions.end());

	for (const auto& extension : availableExtensions)
	{
			requiredExtensions.erase(extension.extensionName);
	}

	return requiredExtensions.empty();
}
PhysicalDevice::SwapChainSupportDetails PhysicalDevice::querySwapChainSupport(VkPhysicalDevice device,VkSurfaceKHR surface)
{
	SwapChainSupportDetails details;
	vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device, surface, &details.capabilities);
	uint32_t formatCount;
	vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &formatCount, nullptr);
	if(formatCount != 0)
	{
		details.formats.resize(formatCount);
		vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &formatCount, details.formats.data());
	}
	uint32_t presentModeCount;
	vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &presentModeCount, nullptr);

	if (presentModeCount != 0)
	{
		details.presentModes.resize(presentModeCount);
		vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &presentModeCount, details.presentModes.data());
	}
	return details;
}
VkSurfaceFormatKHR PhysicalDevice::chooseSwapSurfaceFormat(const std::vector<VkSurfaceFormatKHR>& availableFormats)
{
	if (availableFormats.size() == 1 && availableFormats[0].format == VK_FORMAT_UNDEFINED)
	{
		return {VK_FORMAT_B8G8R8A8_UNORM, VK_COLOR_SPACE_SRGB_NONLINEAR_KHR};
	}
	for (const auto& availableFormat : availableFormats)
	{
		if (availableFormat.format == VK_FORMAT_B8G8R8A8_UNORM && availableFormat.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR)
		{
				return availableFormat;
		}
	}
	return availableFormats[0];
}
VkPresentModeKHR PhysicalDevice::chooseSwapPresentMode(const std::vector<VkPresentModeKHR>& availablePresentModes)
{
	VkPresentModeKHR bestMode = VK_PRESENT_MODE_FIFO_KHR;
	for (const auto& availablePresentMode : availablePresentModes)
	{
		if (availablePresentMode == VK_PRESENT_MODE_MAILBOX_KHR)
		{
			return availablePresentMode;
		}
		else if (availablePresentMode == VK_PRESENT_MODE_IMMEDIATE_KHR)
		{
			bestMode = availablePresentMode;
		}
	}
	return bestMode;
}
VkExtent2D PhysicalDevice::chooseSwapExtent(const VkSurfaceCapabilitiesKHR& capabilities,uint32_t WIDTH, uint32_t HEIGHT)
{
	if (capabilities.currentExtent.width != std::numeric_limits<uint32_t>::max())
	{
		return capabilities.currentExtent;
	}
	else {
		VkExtent2D actualExtent = {WIDTH, HEIGHT};

		actualExtent.width = std::max(capabilities.minImageExtent.width, std::min(capabilities.maxImageExtent.width, actualExtent.width));
		actualExtent.height = std::max(capabilities.minImageExtent.height, std::min(capabilities.maxImageExtent.height, actualExtent.height));

		return actualExtent;
	}
}
PhysicalDevice::SwapChainDetails PhysicalDevice::getSwapDetails(VkPhysicalDevice device,VkSurfaceKHR surface,uint32_t WIDTH, uint32_t HEIGHT)
{
	SwapChainSupportDetails details = querySwapChainSupport(device,surface);
	SwapChainDetails temp;
	temp.format = chooseSwapSurfaceFormat(details.formats);
	temp.presentMode = chooseSwapPresentMode(details.presentModes);
	temp.extent = chooseSwapExtent(details.capabilities,WIDTH,HEIGHT);
	temp.capabilities = details.capabilities;
	return temp;
}
PhysicalDevice::SwapChainDetails PhysicalDevice::getSwapDetailsThis(AppContainer& app)
{
	return getSwapDetails(physicalDevice,app.getSurface(),app.getWidth(),app.getHeight());
}
PhysicalDevice::SwapChainDetails PhysicalDevice::getSwapDetailsThis(AppContainer* app)
{
	return getSwapDetails(physicalDevice,app->getSurface(),app->getWidth(),app->getHeight());
}
PhysicalDevice::QueueFamilyIndices PhysicalDevice::findQueueFamiliesForThis(VkSurfaceKHR surface)
{
	return findQueueFamilies(physicalDevice,surface);
}
PhysicalDevice::QueueFamilyIndices PhysicalDevice::findQueueFamiliesForThis(AppContainer& app)
{
	return findQueueFamilies(physicalDevice,app.getSurface());
}
PhysicalDevice::QueueFamilyIndices PhysicalDevice::findQueueFamiliesForThis(AppContainer* app)
{
	return findQueueFamilies(physicalDevice,app->getSurface());
}
const VkPhysicalDeviceMemoryProperties& PhysicalDevice::getMemoryProperties() const
{
	return memProperties;
}
uint32_t PhysicalDevice::findMemoryType(uint32_t typeFilter, VkMemoryPropertyFlags properties)
{
	for (uint32_t i = 0; i < memProperties.memoryTypeCount; i++) {
		if ((typeFilter & (1 << i)) && (memProperties.memoryTypes[i].propertyFlags & properties) == properties)
		{
				return i;
		}
	}
	throw std::runtime_error("Failed to find suitable memory type!");
}

}
