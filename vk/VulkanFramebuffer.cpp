#include "VulkanFramebuffer.hpp"

namespace Vk {

Framebuffer::Framebuffer(sRenderPass renderPass,const VkImageView* attachments,uint32_t attachmentCount,uint32_t width,uint32_t height)
{
	IF_DEBUG(
		std::cout << "Vk::Framebuffer[" << static_cast<void*>(this) << "]: Creating framebuffer." << std::endl;
	)
	VkFramebufferCreateInfo framebufferInfo = {};
	framebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
	framebufferInfo.renderPass = renderPass->getRenderPass();
	framebufferInfo.attachmentCount = attachmentCount;
	framebufferInfo.pAttachments = attachments;
	framebufferInfo.width = width;
	framebufferInfo.height = height;
	framebufferInfo.layers = 1;
	if (vkCreateFramebuffer(getLogicalDevice(), &framebufferInfo, nullptr, &frameBuffer) != VK_SUCCESS) {
		throw std::runtime_error("Failed to create framebuffer!");
	}
	IF_DEBUG(
		std::cout << "Vk::Framebuffer[" << static_cast<void*>(this) << "]: Successfully created framebuffer." << std::endl;
	)
}
Framebuffer::~Framebuffer()
{
	IF_DEBUG(
		std::cout << "Vk::Framebuffer[" << static_cast<void*>(this) << "]: Destroying framebuffer." << std::endl;
	)
	vkDestroyFramebuffer(getLogicalDevice(), frameBuffer, nullptr);
	IF_DEBUG(
		std::cout << "Vk::Framebuffer[" << static_cast<void*>(this) << "]: Destroyed framebuffer." << std::endl;
	)
}
const VkFramebuffer& Framebuffer::getFramebuffer() const
{
	return frameBuffer;
}
VkFramebuffer* Framebuffer::getFramebufferAddr()
{
	return &frameBuffer;
}
sFramebuffer Framebuffer::create(sRenderPass renderPass,const VkImageView* attachments,uint32_t attachmentCount,uint32_t width,uint32_t height)
{
	return sFramebuffer(new Framebuffer(renderPass,attachments,attachmentCount,width,height) );
}
sFramebuffer Framebuffer::create(sRenderPass renderPass,const VkImageView* attachment,uint32_t width,uint32_t height)
{
	return create(renderPass,attachment,1,width,height);
}
sFramebuffer Framebuffer::create(sRenderPass renderPass,const std::vector<VkImageView>& attachments,uint32_t width,uint32_t height)
{
	return create(renderPass,attachments.data(),attachments.size(),width,height);
}
sFramebuffer Framebuffer::create(sRenderPass renderPass,const VkImageView* attachments,uint32_t attachmentCount,sSwapChain swapchain)
{
	return create(renderPass,attachments,attachmentCount,swapchain->getDetails().extent.width,swapchain->getDetails().extent.height);
}
sFramebuffer Framebuffer::create(sRenderPass renderPass,const VkImageView* attachment,sSwapChain swapchain)
{
	return create(renderPass,attachment,swapchain->getDetails().extent.width,swapchain->getDetails().extent.height);
}
sFramebuffer Framebuffer::create(sRenderPass renderPass,const std::vector<VkImageView>& attachments,sSwapChain swapchain)
{
	return create(renderPass,attachments,swapchain->getDetails().extent.width,swapchain->getDetails().extent.height);
}

}
