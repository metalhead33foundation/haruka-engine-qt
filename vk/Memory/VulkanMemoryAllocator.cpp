#include "VulkanMemoryAllocator.hpp"
namespace Vk {

VulkanMemoryAllocator::VulkanMemoryAllocator(const VkMemoryRequirements& memRequirements, VkMemoryPropertyFlags properties, const void* addr)
	: memSpace(memRequirements,properties,addr), memorySystem(memRequirements.size)
{
	;
}
VkDeviceSize VulkanMemoryAllocator::getRoundedSize(VkDeviceSize intendedSize)
{
	if(intendedSize % memSpace.getMemReq().alignment)
	{
		return ((intendedSize/memSpace.getMemReq().alignment)+1)*memSpace.getMemReq().alignment;
	}
	else return intendedSize;
}
size_t VulkanMemoryAllocator::measureOffsetUntil(MemoryPartition::pChunk chunkAddr)
{
	return memorySystem.measureOffsetUntil(chunkAddr);
}
MemoryPosition VulkanMemoryAllocator::getPosition(MemoryPartition::pChunk chunkAddr)
{
	return memorySystem.getPosition(chunkAddr);
}
MemoryPartition::pChunk VulkanMemoryAllocator::claimMemory(size_t size)
{
	return memorySystem.claimMemory(getRoundedSize(size));
}
void VulkanMemoryAllocator::absorbFreed(MemoryPartition::pChunk chunkAddr)
{
	memorySystem.absorbFreed(chunkAddr);
}
void VulkanMemoryAllocator::free(MemoryPartition::pChunk chunkAddr)
{
	chunkAddr->element.free = true;
	memorySystem.absorbFreed(chunkAddr);
}

const bool VulkanMemoryAllocator::isMapped() const
{
	return memSpace.isMapped();
}
void VulkanMemoryAllocator::waitUntilFreed()
{
	memSpace.waitUntilFreed();
}
void* VulkanMemoryAllocator::startMapping(size_t size, size_t offset)
{
	return memSpace.startMapping(size,getRoundedSize(offset));
}
void* VulkanMemoryAllocator::startMapping(MemoryPosition& pos)
{
	return memSpace.startMapping(pos.size,pos.offset);
}
void* VulkanMemoryAllocator::startMapping(MemoryPartition::pChunk pos)
{
	MemoryPosition mempos=memorySystem.getPosition(pos);
	return startMapping(mempos);
}
void VulkanMemoryAllocator::endMapping()
{
	memSpace.endMapping();
}

}
