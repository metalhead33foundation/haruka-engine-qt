#include "VulkanMemory.hpp"

namespace Vk {

const bool Memory::isMapped() const
{
	return mapped;
}
Memory::Memory(const VkMemoryRequirements& memRequirements, VkMemoryPropertyFlags properties, const void* addr)
	: memReq(memRequirements)
{
	mapped = false;
	IF_DEBUG(
				std::cout << "Vk::Memory[" << static_cast<void*>(this) << "]: Memory allocation." << std::endl;
	)
	VkMemoryAllocateInfo allocInfo = {};
	allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
	allocInfo.allocationSize = memRequirements.size;
	IF_DEBUG(
				std::cout << "Vk::Memory[" << static_cast<void*>(this) << "]: Intended size: " << memRequirements.size << std::endl;
	)
	//allocInfo.memoryTypeIndex = getPhysicalDevice()->findMemoryType(memRequirements.memoryTypeBits, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
	allocInfo.memoryTypeIndex = getPhysicalDevice()->findMemoryType(memRequirements.memoryTypeBits, properties);
	if (vkAllocateMemory(getLogicalDevice(), &allocInfo, nullptr, &deviceMemory) != VK_SUCCESS) {
		throw std::runtime_error("Failed to allocate memory!");
	}
	IF_DEBUG(
				std::cout << "Vk::Memory[" << static_cast<void*>(this) << "]: Successfully allocated memory." << std::endl;
	)
	if(addr)
	{
		mapMemory(addr,memRequirements.size,0);
	}
	//vkBindBufferMemory(getLogicalDevice(), iBufferObject, iBufferMemory, 0);
	//void* data;
	//vkMapMemory(getLogicalDevice(), iBufferMemory, 0, bufferInfo.size, 0, &data);
	//memcpy(data, m_buff->GetInitial(), (size_t) bufferInfo.size);
	//vkUnmapMemory(getLogicalDevice(), iBufferMemory);
}
void Memory::mapMemory(const void* addr,size_t size, size_t offset)
{
	while(mapped);
	void* data = startMapping(size,offset);
	memcpy(data,addr,size);
	endMapping();
}
void* Memory::startMapping(size_t size, size_t offset)
{
	IF_DEBUG(
				std::cout << "Vk::Memory[" << static_cast<void*>(this) << "]: Preparing to map memory." << std::endl;
	)
	void* data;
	mapped = true;
	VkResult res = vkMapMemory(getLogicalDevice(), deviceMemory, (VkDeviceSize)offset, (VkDeviceSize)size, 0, &data);
	if(res == VK_SUCCESS)
	{
		IF_DEBUG(
				std::cout << "Vk::Memory[" << static_cast<void*>(this) << "]: Mapped memory address: [" << static_cast<void*>(data) << "]." << std::endl;
				std::cout << "Vk::Memory[" << static_cast<void*>(this) << "]: Mapped memory size: " << size << "." << std::endl;
		)
		return data;
	}
	else
	{
		switch(res)
		{
		case VK_ERROR_OUT_OF_HOST_MEMORY:
			{
				throw std::runtime_error("Failed to map memory - out of host memory.");
				break;
			}
		case VK_ERROR_OUT_OF_DEVICE_MEMORY:
			{
				throw std::runtime_error("Failed to map memory - out of device memory.");
				break;
			}
		default:
			throw std::runtime_error("Failed to map memory.");
		}
	}
}
void Memory::endMapping()
{
	vkUnmapMemory(getLogicalDevice(), deviceMemory);
	mapped = false;
	IF_DEBUG(
				std::cout << "Vk::Memory[" << static_cast<void*>(this) << "]: Unmapped memory." << std::endl;
	)
}
void Memory::waitUntilFreed()
{
	while(mapped);
}
Memory::~Memory()
{
	if(mapped) endMapping();
	IF_DEBUG(
				std::cout << "Vk::Memory[" << static_cast<void*>(this) << "]: Freeing memory." << std::endl;
	)
	vkFreeMemory(getLogicalDevice(), deviceMemory, nullptr);
	IF_DEBUG(
				std::cout << "Vk::Memory[" << static_cast<void*>(this) << "]: Freed memory." << std::endl;
	)
}
const VkDeviceMemory& Memory::getDeviceMemory() const
{
	;
}
VkDeviceMemory* Memory::getDeviceMemoryAddr()
{
	;
}
sMemory Memory::create(const VkMemoryRequirements& memRequirements, VkMemoryPropertyFlags properties, const void* addr)
{
	return sMemory(new Memory(memRequirements,properties,addr));
}
void Memory::MapFile(PhysFs::FileHandle::SharedHandle handle, size_t offset, size_t maxsize)
{
	while(mapped);
	void* data;
	if(maxsize)
	{
		data = startMapping(maxsize,offset);
		if((size_t)handle->pFileLength() < maxsize)
		{
			handle->pRead(data,1,maxsize);
		}
		else handle->pRead(data,1,(size_t)handle->pFileLength());
	}
	else
	{
		data = startMapping((size_t)handle->pFileLength(),offset);
		handle->pRead(data,1,(size_t)handle->pFileLength());
	}
	endMapping();
}
void Memory::MapFile(const char* filepath, size_t offset, size_t maxsize)
{
	PhysFs::FileHandle::SharedHandle handle = PhysFs::FileHandle::openRead(filepath);
	MapFile(handle,offset,maxsize);
}
void Memory::MapFile(const std::string& filepath, size_t offset, size_t maxsize)
{
	MapFile(filepath.c_str(),offset,maxsize);
}

}
