#ifndef VULKANMEMORYPARTITION_HPP
#define VULKANMEMORYPARTITION_HPP
#include "VulkanMemory.hpp"
//#include <pair>
namespace Vk {

struct MemoryPosition
{
	std::size_t size;
	std::size_t offset;
	MemoryPosition();
	MemoryPosition(std::size_t first, std::size_t second);
};
struct MemoryFile
{
	bool free;
	std::size_t size;
	MemoryFile();
	MemoryFile(bool first, std::size_t second);
};

DEFINE_CLASS(MemoryPartition)
class MemoryPartition
{
public:
	// typedef std::pair<bool,std::size_t> Chunk_T; // first = free or not | second = size
	typedef LinkedList<MemoryFile> Chunk;
	typedef Chunk* pChunk;
private:
	//sMemory memspace;
	pChunk first_chunk;
	void refreshFirstChunk();
public:
	size_t measureOffsetUntil(pChunk chunkAddr);
	MemoryPosition getPosition(pChunk chunkAddr);
	//void* mapChunk(pChunk chunkAddr);
	//void unmapChunk();
	pChunk claimMemory(size_t size);
	void mergeWithPrev(pChunk chunkAddr); // Use with caution
	void mergeWithNext(pChunk chunkAddr); // Use with caution
	void absorbFreed(pChunk chunkAddr);
	MemoryPartition(std::size_t size);
	~MemoryPartition();
};

}
#endif // VULKANMEMORYPARTITION_HPP
