#ifndef ABSTRACTVULKANENGINE_HPP
#define ABSTRACTVULKANENGINE_HPP

#include "Device/VulkanAppContainer.hpp"
#include "VulkanResource.hpp"
#include "Pipeline/VulkanSwapChain.hpp"
#include "Pipeline/VulkanGraphicsPipeline.hpp"
#include "Pipeline/VulkanRenderPass.hpp"
#include "VulkanFramebuffer.hpp"
#include "VkCommandBufferer.hpp"

namespace Vk {

class AbstractEngine
{
private:
	sSwapChain swapChain;
	sGraphicsPipeline pipeline;
	FramebufferVector framebuffers;
	sCommandBufferer commands;

	VkSemaphore imageAvailableSemaphore;
	VkSemaphore renderFinishedSemaphore;
protected:
	sAppContainer container;
	void createSemaphores();
	void destroySemaphores();
	void drawFrame();
public:
	AbstractEngine();
	void InitializeVulkan(sAppContainer new_container);
	void DeinitializeVulkan();
	sAppContainer getContainer() const;
};

}
#endif // ABSTRACTVULKANENGINE_HPP
