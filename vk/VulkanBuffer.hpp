#ifndef VULKANBUFFER_HPP
#define VULKANBUFFER_HPP
#include "Memory/VulkanMemory.hpp"
namespace Vk {

DEFINE_CLASS(Buffer)
class Buffer : public Resource
{
private:
	VkBuffer buffer;
	sMemory memory;
public:
	Buffer(VkDeviceSize size, VkBufferUsageFlags usage, VkMemoryPropertyFlags properties, const void* addr=0);
	~Buffer();
	const VkBuffer& getBuffer() const;
	VkBuffer* getBufferAddr();
	const sMemory& getMemory() const;
	void MapMemory(const void* addr, size_t size);
};

}
#endif // VULKANBUFFER_HPP
