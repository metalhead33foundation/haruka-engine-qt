#include "VkCommandBufferer.hpp"

namespace Vk {

CommandBufferer::CommandBufferer(pAppContainer container,sSwapChain swapChain, sGraphicsPipeline pipeline,const FramebufferVector& framebuffer)
{
	IF_DEBUG(
		std::cout << "Vk::CommandBufferer[" << static_cast<void*>(this) << "]: Creating command bufferer." << std::endl;
	)
	PhysicalDevice::QueueFamilyIndices queueFamilyIndices = getPhysicalDevice()->findQueueFamiliesForThis(container);

	VkCommandPoolCreateInfo poolInfo = {};
	poolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
	poolInfo.queueFamilyIndex = queueFamilyIndices.graphicsFamily;

	if (vkCreateCommandPool(getLogicalDevice(), &poolInfo, nullptr, &commandPool) != VK_SUCCESS) {
			throw std::runtime_error("Failed to create command pool!");
	}
	commandBuffers.resize(framebuffer.size());

	VkCommandBufferAllocateInfo allocInfo = {};
	allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	allocInfo.commandPool = commandPool;
	allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	allocInfo.commandBufferCount = (uint32_t) commandBuffers.size();

	if (vkAllocateCommandBuffers(getLogicalDevice(), &allocInfo, commandBuffers.data()) != VK_SUCCESS) {
		throw std::runtime_error("Failed to allocate command buffers!");
	}

	VkCommandBufferBeginInfo beginInfo = {};
	beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	beginInfo.flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;
	VkRenderPassBeginInfo renderPassInfo = {};
	renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
	VkClearValue clearColor = {0.0f, 0.0f, 0.0f, 1.0f};
	for (size_t i = 0; i < commandBuffers.size(); i++) {

	vkBeginCommandBuffer(commandBuffers[i], &beginInfo);

	renderPassInfo.renderPass = pipeline->getRenderPass()->getRenderPass();
	renderPassInfo.framebuffer = framebuffer[i]->getFramebuffer();
	renderPassInfo.renderArea.offset = {0, 0};
	renderPassInfo.renderArea.extent = swapChain->getDetails().extent;

	renderPassInfo.clearValueCount = 1;
	renderPassInfo.pClearValues = &clearColor;

	vkCmdBeginRenderPass(commandBuffers[i], &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);

	vkCmdBindPipeline(commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline->getPipeline());

	vkCmdDraw(commandBuffers[i], 3, 1, 0, 0);

	vkCmdEndRenderPass(commandBuffers[i]);

	if (vkEndCommandBuffer(commandBuffers[i]) != VK_SUCCESS) {
		throw std::runtime_error("Failed to record command buffer!");
		}
	}

	IF_DEBUG(
		std::cout << "Vk::CommandBufferer[" << static_cast<void*>(this) << "]: Successfully created command bufferer." << std::endl;
	)
}
CommandBufferer::~CommandBufferer()
{
	/*for(BufferIterator it = commandBuffers.begin();it != commandBuffers.end();++it)
	{
		//vkDestroyBuffer(getLogicalDevice(),*it,NULL);
	}*/
	commandBuffers.clear();
	vkDestroyCommandPool(getLogicalDevice(),commandPool,NULL);
}
const VkCommandPool& CommandBufferer::getCommandPool() const
{
	return commandPool;
}
const std::vector<VkCommandBuffer>& CommandBufferer::getCommandBuffers() const
{
	return commandBuffers;
}
VkCommandPool* CommandBufferer::getCommandPoolAddr()
{
	return &commandPool;
}
std::vector<VkCommandBuffer>* CommandBufferer::getCommandBuffersAddr()
{
	return &commandBuffers;
}
sCommandBufferer CommandBufferer::create(pAppContainer container, sSwapChain swapChain, sGraphicsPipeline pipeline,const FramebufferVector& framebuffer)
{
	return sCommandBufferer(new CommandBufferer(container,swapChain,pipeline,framebuffer));
}

}
