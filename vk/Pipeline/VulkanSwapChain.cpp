#include "VulkanSwapChain.hpp"
namespace Vk {

SwapChain::SwapChain(AppContainer* container)
{
	IF_DEBUG(
		std::cout << "Vk::SwapChain[" << static_cast<void*>(this) << "]: Preparing swap chain." << std::endl;
	)
	details = getPhysicalDevice()->getSwapDetailsThis(container);
	uint32_t imageCount = details.capabilities.minImageCount + 1;
	if (details.capabilities.maxImageCount > 0 && imageCount > details.capabilities.maxImageCount) {
				imageCount = details.capabilities.maxImageCount;
	}
	VkSwapchainCreateInfoKHR createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
	createInfo.surface = container->getSurface();

	createInfo.minImageCount = imageCount;
	createInfo.imageFormat = details.format.format;
	createInfo.imageColorSpace = details.format.colorSpace;
	createInfo.imageExtent = details.extent;
	createInfo.imageArrayLayers = 1;
	createInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;

	PhysicalDevice::QueueFamilyIndices indices = getPhysicalDevice()->findQueueFamiliesForThis(container);
	uint32_t queueFamilyIndices[] = {(uint32_t) indices.graphicsFamily, (uint32_t) indices.presentFamily};

	if (indices.graphicsFamily != indices.presentFamily) {
		createInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
		createInfo.queueFamilyIndexCount = 2;
		createInfo.pQueueFamilyIndices = queueFamilyIndices;
	} else {
		createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
	}

	createInfo.preTransform = details.capabilities.currentTransform;
	createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
	createInfo.presentMode = details.presentMode;
	createInfo.clipped = VK_TRUE;

	createInfo.oldSwapchain = VK_NULL_HANDLE;

	if (vkCreateSwapchainKHR(getLogicalDevice(), &createInfo, nullptr, &swapChain) != VK_SUCCESS) {
		throw std::runtime_error("Failed to create swap chain!");
	}
	IF_DEBUG(
				std::cout << "Vk::SwapChain[" << static_cast<void*>(this) << "]: Successfully created swap chain. Now creating image views." << std::endl;
	)
	vkGetSwapchainImagesKHR(getLogicalDevice(), swapChain, &imageCount, nullptr);
	swapChainImages.resize(imageCount);
	vkGetSwapchainImagesKHR(getLogicalDevice(), swapChain, &imageCount, swapChainImages.data());
	swapChainImageViews.resize(swapChainImages.size());
	for (size_t i = 0; i < swapChainImages.size(); i++) {
		VkImageViewCreateInfo createInfo = {};
		createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
		createInfo.image = swapChainImages[i];
		createInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
		createInfo.format = details.format.format;
		createInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
		createInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
		createInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
		createInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
		createInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		createInfo.subresourceRange.baseMipLevel = 0;
		createInfo.subresourceRange.levelCount = 1;
		createInfo.subresourceRange.baseArrayLayer = 0;
		createInfo.subresourceRange.layerCount = 1;

		if (vkCreateImageView(getLogicalDevice(), &createInfo, nullptr, &swapChainImageViews[i]) != VK_SUCCESS) {
			throw std::runtime_error("Failed to create image views!");
		}
	}
	IF_DEBUG(
				std::cout << "Vk::SwapChain[" << static_cast<void*>(this) << "]: Successfully created image views." << std::endl;
	)
}
SwapChain::~SwapChain()
{

	IF_DEBUG(
				std::cout << "Vk::SwapChain[" << static_cast<void*>(this) << "]: Destroying image views." << std::endl;
	)
	for (size_t i = 0; i < swapChainImageViews.size(); i++)
	{
		IF_DEBUG(
					std::cout << "Vk::SwapChain[" << static_cast<void*>(this) << "]: Destroying the image " << i << std::endl;
		)
		vkDestroyImageView(getLogicalDevice(), swapChainImageViews[i], nullptr);
	}
	IF_DEBUG(
				std::cout << "Vk::SwapChain[" << static_cast<void*>(this) << "]: Successfully destroyed image views. Now destroying the swap chain itself." << std::endl;
	)
	vkDestroySwapchainKHR(getLogicalDevice(), swapChain, nullptr);

	IF_DEBUG(
				std::cout << "Vk::SwapChain[" << static_cast<void*>(this) << "]: Destroyed the swap chain." << std::endl;
	)
}

sSwapChain SwapChain::create(AppContainer* container)
{
	return sSwapChain(new SwapChain(container));
}
const VkSwapchainKHR& SwapChain::getSwapChain() const
{
	return swapChain;
}
VkSwapchainKHR* SwapChain::getSwapChainAddr()
{
	return &swapChain;
}
const PhysicalDevice::SwapChainDetails& SwapChain::getDetails() const
{
	return details;
}
PhysicalDevice::SwapChainDetails* SwapChain::getDetailsAddr()
{
	return &details;
}
const std::vector<VkImage>& SwapChain::getSwapChainImages() const
{
	return swapChainImages;
}
const std::vector<VkImageView>& SwapChain::getSwapChainImageViews() const
{
	return swapChainImageViews;
}
std::vector<VkImage>* SwapChain::getSwapChainImagesAddr()
{
	return &swapChainImages;
}
std::vector<VkImageView>* SwapChain::getSwapChainImageViewsAddr()
{
	return &swapChainImageViews;
}
/*const std::vector<VkFramebuffer>& SwapChain::getSwapChainFramebuffers() const
{
	return swapChainFramebuffers;
}
std::vector<VkFramebuffer>* SwapChain::getSwapChainFramebuffersAddr()
{
	return &swapChainFramebuffers;
}*/

}
