#include "VulkanShaderModule.hpp"
#include "../../PhysFsWrappers/Physfs4Cpp.hpp"
#include <cstring>

namespace Vk {


ShaderModule::ShaderModule(const std::vector<char>& code)
{
	prepareShaderModule(code);
}
ShaderModule::ShaderModule(std::string path)
{
	prepareShaderModule(path);
}
sShaderModule ShaderModule::create(const std::vector<char>& code)
{
	return sShaderModule(new ShaderModule(code));
}
sShaderModule ShaderModule::create(std::string path)
{
	return sShaderModule(new ShaderModule(path));
}
ShaderModule::~ShaderModule()
{
	vkDestroyShaderModule(getLogicalDevice(), shaderModule, nullptr);
}
void ShaderModule::prepareShaderModule(const std::vector<char>& code)
{
	IF_DEBUG(
				std::cout << "Vk::ShaderModule[" << static_cast<void*>(this) << "]: Preparing shader module." << std::endl;
	)
	VkShaderModuleCreateInfo createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
	createInfo.codeSize = code.size();

	std::vector<uint32_t> codeAligned(code.size() / sizeof(uint32_t) + 1);
	memcpy(codeAligned.data(), code.data(), code.size());
	createInfo.pCode = codeAligned.data();
	if (vkCreateShaderModule(getLogicalDevice(), &createInfo, nullptr, &shaderModule) != VK_SUCCESS) {
		throw std::runtime_error("Failed to create shader module!");
	}
	IF_DEBUG(
		std::cout << "Vk::ShaderModule[" << static_cast<void*>(this) << "]: Succesfully prepared shader module." << std::endl;
	)
}
void ShaderModule::prepareShaderModule(std::string path)
{
	prepareShaderModule(PhysFs::FileHandle::fileToBuffer(path));
}
const VkShaderModule& ShaderModule::getModule() const
{
	return shaderModule;
}
VkShaderModule* ShaderModule::getModuleAddr()
{
	return &shaderModule;
}
VkPipelineShaderStageCreateInfo ShaderModule::stage(VkShaderStageFlagBits stage, const char* name)
{
	VkPipelineShaderStageCreateInfo temp;
	temp.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
	temp.stage = stage;
	temp.module = shaderModule;
	temp.pName = name;
	return temp;
}

}
