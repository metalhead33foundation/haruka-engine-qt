#ifndef VULKANPIPELINELAYOUT_HPP
#define VULKANPIPELINELAYOUT_HPP
#include "VulkanSwapChain.hpp"
namespace Vk {

DEFINE_CLASS(PipelineLayout)
class PipelineLayout : public Resource
{
private:
	VkPipelineLayout pipelineLayout;
public:
	const VkPipelineLayout& getPipelineLayout() const;
	VkPipelineLayout* getPipelineLayoutAddr();
	PipelineLayout();
	static sPipelineLayout create();
	~PipelineLayout();
};

}
#endif // VULKANPIPELINELAYOUT_HPP
