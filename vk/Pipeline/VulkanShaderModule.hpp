#ifndef VULKANSHADERMODULE_HPP
#define VULKANSHADERMODULE_HPP
#include "../VulkanResource.hpp"
namespace Vk {

DEFINE_CLASS(ShaderModule)
class ShaderModule : public Resource
{
private:
	VkShaderModule shaderModule;
	void prepareShaderModule(const std::vector<char>& code);
	void prepareShaderModule(std::string path);
protected:
	ShaderModule(const std::vector<char>& code);
	ShaderModule(std::string path);
public:
	~ShaderModule();
	static sShaderModule create(const std::vector<char>& code);
	static sShaderModule create(std::string path);

	const VkShaderModule& getModule() const;
	VkShaderModule* getModuleAddr();
	VkPipelineShaderStageCreateInfo stage(VkShaderStageFlagBits stage, const char* name);
};

}
#endif // VULKANSHADERMODULE_HPP
