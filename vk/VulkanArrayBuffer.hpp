#ifndef VULKANARRAYBUFFER_HPP
#define VULKANARRAYBUFFER_HPP
#include "../global/UploadableGpuArray.hpp"
#include "Memory/VulkanMemory.hpp"
namespace Vk {

DEFINE_CLASS(ArrayBuffer)
class ArrayBuffer : public Resource
{
	UploadableGpuArray* m_buff;
protected:
	//void GenerateBuffer();
	VkBuffer iBufferObject; // Buffer object
	sMemory iBufferMemory;
	VkBufferUsageFlagBits flag;
	size_t size;
	size_t elems;
public:
	ArrayBuffer(VkBufferUsageFlagBits n_flag, UploadableGpuArray* n_buff);
	~ArrayBuffer();
	const VkBuffer& GetBufferObject() const;
	sMemory getMemory() const;
	void GenerateAllInOne();

	//void Bind();
	virtual void OffloadToBuffer();
	size_t GetElemnum() const;
	const VkBufferUsageFlagBits& getFlag() const;
	//void GenerateAllInOne();

	//virtual void Draw() = 0;
};

}
#endif // VULKANARRAYBUFFER_HPP
