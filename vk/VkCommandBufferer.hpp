#ifndef VKCOMMANDBUFFERER_HPP
#define VKCOMMANDBUFFERER_HPP
#include "VulkanResource.hpp"
#include "VulkanFramebuffer.hpp"
#include "Pipeline/VulkanGraphicsPipeline.hpp"
namespace Vk {

DEFINE_CLASS(CommandBufferer)
class CommandBufferer : public Resource
{
private:
	VkCommandPool commandPool;
	typedef std::vector<VkCommandBuffer> BufferVector;
	typedef BufferVector::iterator BufferIterator;
	BufferVector commandBuffers;
public:
	CommandBufferer(pAppContainer container, sSwapChain swapChain, sGraphicsPipeline pipeline,const FramebufferVector& framebuffer);
	~CommandBufferer();
	const VkCommandPool& getCommandPool() const;
	const std::vector<VkCommandBuffer>& getCommandBuffers() const;
	VkCommandPool* getCommandPoolAddr();
	std::vector<VkCommandBuffer>* getCommandBuffersAddr();

	static sCommandBufferer create(pAppContainer container, sSwapChain swapChain, sGraphicsPipeline pipeline,const FramebufferVector& framebuffer);
};

}
#endif // VKCOMMANDBUFFERER_HPP
