#include "QtVkEngine.hpp"

const std::string QtVkEngine::ENGINE_NAME = "Haruka Engine";
const uint32_t QtVkEngine::ENGINE_VERSION = VK_MAKE_VERSION(1,0,0);

QtVkEngine::QtVkEngine(QWidget* handle, const std::string& name, uint32_t version)
{
	appname = name;
	app_version = version;
	m_window = handle;
}
bool QtVkEngine::Initialize()
{
	CStringVector desired_extensions(0);
	InitializeVulkan( QtVkContainer::createCast(m_window,ENGINE_NAME,ENGINE_VERSION,appname,app_version,desired_extensions) );
	return true;
}
void QtVkEngine::Render(sf::Time deltaTime)
{
	if(!m_window->isEnabled() ) exit_signal = true;
	if(!exit_signal)
	{
			//glfwPollEvents();
			drawFrame();
	}
}
void QtVkEngine::Deinitialize()
{
	DeinitializeVulkan();
}
