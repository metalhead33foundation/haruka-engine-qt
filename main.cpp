#include "widget.h"
#include <QApplication>
#include "QtVkEngine.hpp"
#include "PhysFsWrappers/Physfs4Cpp.hpp"

const std::string appname = "Test App";

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	PhysFs::FileHandle::Initialize(argv[0]);
	InitVulkan();
	std::ifstream infile("mountlist.txt");
	std::string line;
	while (std::getline(infile, line))
	{
		PhysFs::FileHandle::Mount(line);
	}
	PhysFs::FileHandle::Mount(PhysFs::FileHandle::GetBaseDir() + PhysFs::FileHandle::GetDirSeparator() + "data" );
	Widget w;
	QtVkEngine app(&w,appname,VK_MAKE_VERSION(0,0,0));
	w.show();
	app.Run();

	return a.exec();
}
