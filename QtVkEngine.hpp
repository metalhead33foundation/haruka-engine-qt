#ifndef QTVKENGINE_HPP
#define QTVKENGINE_HPP
#include "QtVkContainer.hpp"
#include "global/AbstractRenderingEngine.hpp"
#include "vk/AbstractVulkanEngine.hpp"

class QtVkEngine : public AbstractRenderingEngine, public Vk::AbstractEngine
{
private:
	QWidget* m_window;
	std::string appname;
	uint32_t app_version;
public:
	QtVkEngine(QWidget* handle, const std::string& name, uint32_t version);
	virtual bool Initialize();
	virtual void Render(sf::Time deltaTime);
	virtual void Deinitialize();
	static const std::string ENGINE_NAME;
	static const uint32_t ENGINE_VERSION;
};

#endif // QTVKENGINE_HPP
